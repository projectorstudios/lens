module.exports = function(hook, token){
	return new lensInstance(hook, token);
}

var lensInstance = function(hook, token){
	this.needle = require('needle');
	this.url = hook+'?token='+token;
	
	this.logInfo = function(title, data) {
		console.info("[INFO] "+title+": "+data);
		
		this.logSlackInfo(title, data);
	}
	
	this.logWarning = function(title, data) {
		console.warn("[WARNING] "+title+": "+data);
		
		this.logSlackWarning(title, data);
	}
	
	this.logError = function(title, data) {
		console.error("[ERROR] "+title+": "+data);
		
		this.logSlackError(title, data);
	}
	
	this.logSlackInfo = function(title, data) {
		var params = {
			'fallback':'[INFO] '+title+': '+data,
			'pretext':'[INFO]',
			'color':'good',
			'fields':[{
				'title':title,
				'value':data,
				'short':false
			}]
		};

		this.postLog(this.url, JSON.stringify(params));
	}
	
	
	this.logSlackWarning = function(title, data) {
		var params = {
			'fallback':'[WARN] '+title+': '+data,
			'pretext':'[WARN]',
			'color':'warning',
			'fields':[{
				'title':title,
				'value':data,
				'short':false
			}]
		};
	
		this.postLog(this.url, JSON.stringify(params));
	}

	this.logSlackError = function(title, data) {
		var params = {
			'fallback':'[ERROR] '+title+': '+data,
			'pretext':'[ERROR]',
			'color':'danger',
			'fields':[{
				'title':title,
				'value':data,
				'short':false
			}]
		};

		this.postLog(this.url, JSON.stringify(params));
	}
	
	this.postLog = function(url, params) {
		this.needle.post(url, params);
	}
}