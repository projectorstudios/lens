# Lens #

Node.js logging module that publishes to the console and to a Slack room

### Installation ###

    npm install git+https://projector@bitbucket.org/projector/lens.git

### Usage ###

## To create a Lens instance ##
```
#!javascript
var myhook = "https://mycompany.slack.com/services/hooks/incoming-webhook";
var mytoken = "abcdefg123456";
var lens = require('lens')(myhook, mytoken);
```

## To log output ##
```
#!javascript
lens.logInfo("MyInfo", "My informational message");

lens.logWarning("MyWarning", "My warning message");

lens.logError("MyError", "My error message");
```